# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is for AGL code test. 
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Technologies Used? ### 

Angular 5
Typescript 2.x
NodeJs
Bootstrap 4
Html5
RxJs
Jasmine
.......

### How do I get set up? ###

Please download the code and open it using VSCode or any prefered IDE then run npm install to install node modules
then run npm start or if you have already installed Angular-cli then type ng serve -o to open in browser.

### Who do I talk to? ###
Naming convention can be changed based on given company coding standards. Some use camelCase and others use PascalCase. However, one pattern must be used.  
* Repo owner or admin - Pri Gunawardana
* Other community or team contact