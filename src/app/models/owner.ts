import { Pet } from "./pet";

export class Owner{
    name:string;
    age:number;
    gender:string;
    pets:Pet[]
}