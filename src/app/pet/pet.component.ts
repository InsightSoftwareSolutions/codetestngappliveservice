import { Component, OnInit } from '@angular/core';

import { PetService } from '../services/pet.service';

import { PetsUnderHeadingOfGender } from '../models/petsunderheadingofgender';
import { Pet } from '../models/pet';


@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css']
})
export class PetComponent implements OnInit {

  petsUnderHeadingOfGender: PetsUnderHeadingOfGender[] = [];

  constructor(private petService: PetService) { }

  ngOnInit() {

    this.petService.getOwnersByPetType()
      .subscribe(owners => {

        let pets: Pet[] = [];

        let maleOwners = owners.filter(m => m.gender == "Male" && m.pets);
        let femaleOwners = owners.filter(f => f.gender == "Female" && f.pets);
        
        maleOwners.forEach(maleOwner => {
          pets = pets.concat(maleOwner.pets);
        });
        this.petsUnderHeadingOfGender.push({ gender: "Male", pets: pets });

        pets = [];
        
        femaleOwners.forEach(femaleOwner => {
          pets = pets.concat(femaleOwner.pets);
        });
        this.petsUnderHeadingOfGender.push({ gender: "Female", pets: pets });


      });
  }

}
