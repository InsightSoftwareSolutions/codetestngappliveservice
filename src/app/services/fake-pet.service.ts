import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

// re-export for tester convenience
export { Pet } from '../models/pet';
export { Owner } from '../models/owner';
export { PetService } from '../services/pet.service';

import { Pet } from '../models/pet';
import { Owner } from '../models/owner';
import { PetService } from '../services/pet.service';

export const OWNERS: Owner[] = [{ name: "Bob", gender: "Male", age: 23, "pets": [{ name: "Garfield", type: "Cat" }, { name: "Fido", type: "Dog" }] }, { name: "Jennifer", gender: "Female", age: 18, "pets": [{ name: "Garfield", type: "Cat" }] }, { name: "Steve", gender: "Male", age: 45, "pets": null }, { name: "Fred", gender: "Male", age: 40, "pets": [{ name: "Tom", type: "Cat" }, { name: "Max", type: "Cat" }, { name: "Sam", type: "Dog" }, { name: "Jim", type: "Cat" }] }, { name: "Samantha", gender: "Female", age: 40, "pets": [{ name: "Tabby", type: "Cat" }] }, { name: "Alice", gender: "Female", age: 64, "pets": [{ name: "Simba", type: "Cat" }, { name: "Nemo", type: "Fish" }] }];

export class FakePetService {

  owners = OWNERS;

  getOwnersByPetType(): Observable<Owner[]> {

    let _owneres = this.owners;
    return Observable.of(_owneres);
  }

}
